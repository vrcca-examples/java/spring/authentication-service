#!/bin/sh -e
PROFILE=${1:-"default"}

echo "Starting app..."
java -jar -Dspring.profiles.active=$PROFILE \
          -DENCRYPT_KEY=$ENCRYPT_KEY \
          app.jar
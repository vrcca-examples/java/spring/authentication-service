package com.thoughtmechanix.controllers;

import com.thoughtmechanix.utils.UserContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping
    public Map<String, Object> user(OAuth2Authentication user) {
        logRequestsFor("/user");
        final Map<String, Object> userInfo = new HashMap<>();
        final Authentication userAuthentication = user.getUserAuthentication();
        userInfo.put("user", userAuthentication.getPrincipal());
        userInfo.put("authorities", AuthorityUtils.authorityListToSet(userAuthentication.getAuthorities()));
        return userInfo;
    }

    private void logRequestsFor(String operation) {
        logger.info("{} requesting {}", UserContextHolder.getContext().toString(), operation);
    }

}

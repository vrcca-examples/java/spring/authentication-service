package com.thoughtmechanix.utils;

import java.util.StringJoiner;

public class UserContext {
    public static final UserContext EMPTY = new UserContext("");
    public static final String CORRELATION_ID = "tmx-correlation-id";

    private final String correlationId;

    public UserContext(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public static UserContext empty() {
        return EMPTY;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ")
                .add("correlationId='" + correlationId + "'")
                .toString();
    }
}
